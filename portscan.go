package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"sync"
	"time"
)

type State int

const (
	StateOpen State = iota
	StateClosed
	StateFiltered
)

func tryPort(host string, port int, timeout time.Duration) State {
	addr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.DialTimeout("tcp", addr, timeout)
	if err == nil {
		conn.Close()
		return StateOpen
	}
	if netErr, ok := err.(net.Error); ok {
		if netErr.Timeout() {
			return StateFiltered
		} else {
			return StateClosed
		}
	}
	return StateClosed
}

func main() {
	var concurrency int
	var timeout time.Duration
	var network string

	// TODO automagic ulimit discovery
	// TODO automagic timeout discovery based on icmp echo
	flag.IntVar(&concurrency, "c", 256, "concurrency")
	flag.DurationVar(&timeout, "t", 1000*time.Millisecond, "timeout")
	flag.StringVar(&network, "n", "ip", "network")

	flag.Parse()
	if flag.NArg() != 1 {
		fmt.Fprintf(os.Stderr, "Usage: %s [OPTION ...] host\n",
			os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	pr := NewCCPrint()
	cc := make(chan bool, concurrency)
	var wg sync.WaitGroup

	ports := []int{}
	for i := 0; i < 65536; i++ { // there must be a better way
		ports = append(ports, i)
	}
	hostname := flag.Arg(0)

	ip, err := net.ResolveIPAddr(network, hostname)
	if err != nil {
		panic(err)
	}
	fmt.Fprintln(os.Stderr, "using address", ip.IP.String())

	for _, port := range ports {
		cc <- true
		wg.Add(1)
		go func(port int) {
			defer func() { <-cc }()
			defer wg.Done()
			pr.Status(fmt.Sprintf("%d", port))
			state := tryPort(ip.IP.String(), port, timeout)
			switch state {
			case StateOpen:
				pr.Result(fmt.Sprintf("%d: open", port))
			case StateClosed:
				pr.Result(fmt.Sprintf("%d: closed", port))
			case StateFiltered:
				// pr.Result(fmt.Sprintf("%d: filtered", port))
			}
			<-cc
		}(port)
	}
	wg.Wait()

	pr.End()
}
