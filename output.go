package main

import (
	"fmt"
)

type CCPrint struct {
	sem chan bool
}

func (self *CCPrint) reuseLine() {
	fmt.Print("\033[u\033[K")
}

func (self *CCPrint) savePos() {
	fmt.Print("\033[s")
}

func (self *CCPrint) lock() {
	self.sem <- true
}

func (self *CCPrint) release() {
	<-self.sem
}

func (self *CCPrint) Status(str string) {
	self.lock()
	self.reuseLine()
	fmt.Print(str)
	self.release()
}

func (self *CCPrint) Result(str string) {
	self.lock()
	self.reuseLine()
	fmt.Println(str)
	self.savePos()
	self.release()
}

func (self *CCPrint) End() {
	self.reuseLine()
}

func NewCCPrint() CCPrint {
	sem := make(chan bool, 1)
	self := CCPrint{sem: sem}
	self.savePos()
	return self
}
